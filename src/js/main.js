import documentReady from "./helpers/documentReady";
import lazyImages from "./modules/lazyImages";

documentReady(() => {
	lazyImages();
});
